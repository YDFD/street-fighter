import { showModal } from './modal';
import { createImage } from '../fightersView';

export function showWinnerModal(fighter) {
  // call showModal function 
  showModal({ 
    title: fighter.name, 
    bodyElement: createImage(fighter.source)
  });


}
