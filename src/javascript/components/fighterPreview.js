import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const { attack, defense, health, name } = fighter;
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  const fighterImage = createFighterImage(fighter);
  fighterElement.appendChild(fighterImage);

  const fighterName = createElement({
    tagName: 'h1',
    className: `fighter-preview___root--name ${positionClassName}`,
  });
  fighterName.innerHTML=name;
  fighterElement.appendChild(fighterName);

  const fighterAttack = createElement({
    tagName: 'h1',
    className: `fighter-preview___root--name ${positionClassName}`,
  });
  fighterAttack.innerHTML=attack;
  fighterElement.appendChild(fighterAttack);

  const fighterDefense = createElement({
    tagName: 'h1',
    className: `fighter-preview___root--name ${positionClassName}`,
  });
  fighterDefense.innerHTML=defense;
  fighterElement.appendChild(fighterDefense);

  const fighterHealth = createElement({
    tagName: 'h1',
    className: `fighter-preview___root--name ${positionClassName}`,
  });
  fighterHealth.innerHTML=health;
  fighterElement.appendChild(fighterHealth);
  // todo: show fighter info (image, name, health, etc.)

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
