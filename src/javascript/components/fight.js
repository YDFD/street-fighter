import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
  
    document.addEventListener('keydown', handleKeyDown);

    console.log('starterd');
    
    function handleKeyDown(event) {
      const currentKey = event.code;
    
      switch (currentKey) {
        //Handle first player attacks
        case controls.PlayerOneAttack: {
          const nominalHealth = secondFighter.health;
          const damage = getHitPower(firstFighter);

          const decreasedPercentage = (100 -  ( 100 * damage ) / nominalHealth) ;

          const secondFighterHealth = document.getElementById('right-fighter-indicator');
          
          const lengthOfHealthBarInPercentage = secondFighterHealth.style.width;
          
          if (!lengthOfHealthBarInPercentage) {
            secondFighterHealth.style.width='100%';
          } else {
            secondFighterHealth.style.width=`${secondFighterHealth.style.width.slice(0, -1) - decreasedPercentage}%`;
          }
          console.log('damaged', lengthOfHealthBarInPercentage);
          
        }
        //Handle first player block
        case controls.PlayerOneBlock: {
          
        }
        //Handle second player attacks
        case controls.PlayerTwoAttack: {
          firstFighter.health -= getHitPower(secondFighter);
        }
        //Handle second player block
        case controls.PlayerTwoBlock: {}
      }
    }

    if (firstFighter.health <=0) {
      resolve(secondFighter);
    }
    if (secondFighter.health <= 0) {
      resolve(firstFighter);
    }
  });
}

export function getDamage(attacker, defender) {
  const diff = getHitPower(attacker) - getBlockPower(defender);
  // return damage
  return diff < 0 ? 0 : diff;
}

export function getHitPower(fighter) {
  const criticalHitChance = Math.random() + 1; 
  // return hit power
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  const dodgeChance =  Math.random() + 1; 
  // return block power
  return fighter.defense * dodgeChance;
}
